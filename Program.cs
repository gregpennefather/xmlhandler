﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ConsoleApp1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Setup
            var dog = new Breed() { Name = "Dog" };
            var cat = new Breed() { Name = "Cat" };

            var spike = new Pet() { Breed = dog, Name = "Spike" };
            var boots = new Pet() { Breed = cat, Name = "Boots" };

            var house = new House() { Name = "Home", Pets = new List<Pet>() { spike, boots } };

            // Write
            XmlSerializer xsSubmit = new XmlSerializer(typeof(House));
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, house);
                    xml = sww.ToString(); // Your XML
                }
            }

            Console.WriteLine(xml);
            Console.ReadLine();

            // Read

            XmlSerializer serializer = new XmlSerializer(typeof(House));
            using (TextReader reader = new StringReader(xml))
            {
                House result = (House)serializer.Deserialize(reader);
            }

            Console.WriteLine(house.Name);
            Console.WriteLine(house.InstanceId);
            Console.WriteLine(house.Pets.First().Name);
            Console.WriteLine(house.Pets.Last().Name);
            Console.ReadLine();
        }
    }
}