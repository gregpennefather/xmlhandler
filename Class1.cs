﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class House
    {
        public House()
        {
            InstanceId = Guid.NewGuid();
        }

        public Guid InstanceId { get; set; }

        public string Name { get; set; }

        public List<Pet> Pets { get; set; }
    }

    public class Pet
    {
        public Breed Breed { get; set; }

        public string Name { get; set; }
    }

    public class Breed
    {
        public string Name { get; set; }
    }
}